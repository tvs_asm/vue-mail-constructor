import { CreateElement } from 'vue/types/vue';
import { VNode } from 'vue/types/vnode';
import { DefaultProps, InjectOptions, PropsDefinition, RenderContext } from 'vue/types/options';

declare module 'vue' {
  export interface FunctionalComponentOptions<Props = DefaultProps, PropDefs = PropsDefinition<Props>> {
    name?: string;
    props?: PropDefs;
    model?: {
      prop?: string;
      event?: string;
    };
    inject?: InjectOptions;
    functional: boolean;
    render?(this: undefined, createElement: CreateElement, context: RenderContext<Props>): VNode | VNode[];
    [key: string]: any;
  }
}
