const spaces = [
  '0px',
  '4px',
  '8px',
  '16px',
  '24px',
  '48px',
  '64px',
];

interface Dict {
  [key: string]: any;
}
/**
 *
 * @param prop
 */
function makeStyle (prop: string) {
  const style: Dict = {};
  spaces.forEach((value, index) => {
    style[index] = { [prop]: value };
  });
  return style;
}

export const MT = makeStyle('marginTop');
export const MB = makeStyle('marginBottom');
export const ML = makeStyle('marginLeft');
export const MR = makeStyle('marginRight');

export const PT = makeStyle('paddingTop');
export const PB = makeStyle('paddingBottom');
export const PL = makeStyle('paddingLeft');
export const PR = makeStyle('paddingRight');
