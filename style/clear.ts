export const BASE_CLEAR_TABLE = {
  border: 'none',
  borderSpacing: 0,
  padding: 0,
  width: '100%',
};
