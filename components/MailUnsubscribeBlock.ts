export default {
  functional: true,
  unsubscribeUrl: '{{UnsubscribeUrl}}',
  template: `
  <table class="footer base-clear-table" >
    <tr>
      <td>
        <a :href="$options.unsubscribeUrl"><strong>Отписаться от рассылки</strong></a>
      </td>
    </tr>
  </table>
  `,
};
